module v2raycon

go 1.14

require (
	github.com/fatih/color v1.9.0
	github.com/google/uuid v1.1.1
	github.com/qianlnk/qrcode v0.0.0-20180102074946-a03586d59508
	github.com/skip2/go-qrcode v0.0.0-20191027152451-9434209cb086 // indirect
	github.com/urfave/cli/v2 v2.2.0
)
