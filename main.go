package main

import (
	"github.com/urfave/cli/v2"
	"log"
	"os"
)

func main() {
	app := &cli.App{
		Name:  "v2raycon",
		Usage: "v2ray configure file generate",
		Commands: []*cli.Command{
			{
				Name:  "init",
				Usage: "Generate a template file and add the first user,when you delete the user,you need to re-initialize the configuration file",
				Action: func(context *cli.Context) error {
					return InitTemp()
				},
			},
			{
				Name:  "add",
				Usage: "Add a user",
				Action: func(ctx *cli.Context) error {
					return AddUser(ctx.Args().First())
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
