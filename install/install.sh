#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user" 2>&1
  exit 1
fi

if [ ! -f /opt/v2ray-con/config.json ];then
  # shellcheck disable=SC2164
  mkdir -p /opt/v2ray-con/ && cd /opt/v2ray-con/
  chmod 755 -R /opt/v2ray-con/
  wget https://bitbucket.org/sxuecks/v2raycon/downloads/v2raycon_linux.zip
  unzip v2raycon_linux.zip && rm v2raycon_linux.zip
fi

cat << EOF > v2raycon.service
[Unit]
Description=V2RayCon
Documentation=https://bitbucket.org/sxuecks/v2raycon/src/develop/README.md
After=network.target nss-lookup.target
Wants=network-online.target

[Service]
Type=simple
User=root
CapabilityBoundingSet=CAP_NET_BIND_SERVICE CAP_NET_RAW
NoNewPrivileges=yes
ExecStart=/usr/bin/v2ray/v2ray -config /opt/v2ray-con/config.json
Restart=on-failure
RestartPreventExitStatus=23

[Install]
WantedBy=multi-user.target
EOF

cp v2raycon.service /etc/systemd/system/v2raycon.service
systemctl enable v2raycon.service
systemctl start v2raycon.service
systemctl status v2raycon.service