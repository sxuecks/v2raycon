0. 设置环境变量 `V2Serv = 你的服务器地址`，如果不存在则自动使用你的公网地址
1. ./v2raycon init 初始化生成config.json
2. ./v2raycon add user 建立用户

自动安装：
```$xslt
curl https://bitbucket.org/sxuecks/v2raycon/downloads/install.sh -L | sudo bash -
```
