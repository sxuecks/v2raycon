package main

import (
	"fmt"
	"testing"
)

func TestUserConfigToFile(t *testing.T) {
	var uconfig UserConfig
	uconfig.UserName = "sxueck"
	uconfig.UserPort = 51001
	uconfig.UserUuid = Uuid()
	if err := UserConfigToFile(uconfig);err != nil {
		t.Fatal(err)
	}
}

func TestFileToUserConfig(t *testing.T) {
	uconfig := FileToUserConfig()
	if uconfig == nil {
		t.Fatal()
	}
	fmt.Printf("%+v\n",uconfig)
}

func TestGetCliLink(t *testing.T) {
	GetCliLink("anonymous")
}