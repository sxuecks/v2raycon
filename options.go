package main

import (
	"bufio"
	"encoding/base64"
	"fmt"
	. "github.com/fatih/color"
	"html/template"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
)

type UserConfig struct {
	UserName string //username
	UserPort int
	UserUuid string
}

const StartPort = 51001
const UserList = "userlist"

func InitTemp() error {
	Cyan("Initializing v2ray template file...")
	tmpl := template.Must(template.New("config.json.tmpl").ParseFiles("config.json.tmpl"))

	uconfig := FileToUserConfig()
	if uconfig == nil {
		fmt.Println("There are currently no users or the user list is missing")
		if err := AddUser("anonymous"); err != nil {
			log.Fatal(err)
		}
		uconfig = FileToUserConfig()
	}

	_ = os.Remove("config.json")

	fp := getMeAfp("config.json")
	err := tmpl.Execute(fp, uconfig)

	if err != nil {
		return fmt.Errorf("template execute error :%v", err)
	}
	return nil
}

func getMeAfp(filename string) *os.File {
	fp, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		log.Fatal(err)
	}
	return fp
}

func AddUser(username string) error {
	fmt.Printf("adding %s user...\n", username)
	currentUserCount := cap(FileToUserConfig())
	uconfig := UserConfig{
		UserName: username,
		UserUuid: fmt.Sprintf("%s", Uuid()),
		UserPort: StartPort + currentUserCount,
	}
	if err := UserConfigToFile(uconfig); err != nil {
		return err
	}
	GetCliLink(username)
	if err := InitTemp();err != nil {
		log.Fatalf("add user error:%s",err)
	}
	return nil
}

func UserConfigToFile(uconfig UserConfig) error {
	fp := getMeAfp(UserList)
	defer fp.Close()
	_, err := fp.WriteString(
		fmt.Sprintf("%s %d %s\r\n", uconfig.UserName, uconfig.UserPort, uconfig.UserUuid))
	if err != nil {
		return err
	}
	return nil
}

func FileToUserConfig() []UserConfig {
	fp, err := os.Open(UserList)
	if err != nil {
		return nil
	}

	//按照 username port uuid 的格式来

	defer fp.Close()
	scanner := bufio.NewScanner(fp)
	scanner.Split(bufio.ScanLines)

	var uconfig []UserConfig
	var i = 0

	for scanner.Scan() {
		uinfo := strings.Fields(scanner.Text())
		port, _ := strconv.Atoi(uinfo[1])
		uconfig = append(uconfig, UserConfig{
			UserName: uinfo[0],
			UserPort: port,
			UserUuid: uinfo[2],
		})
		i++
	}

	return uconfig
}

func GetCliLink(username string) {
	uconfig := FileToUserConfig()
	var clilink string
	for _, v := range uconfig {
		if v.UserName == username {
			clilink = "vmess://" + base64.StdEncoding.EncodeToString([]byte(
				fmt.Sprintf("{\"v\":\"2\",\"ps\":\"\",\"add\":\"%s\",\"port\":%d,\"aid\":128,\"type\":\"http\",\"net\":\"tcp\",\"path\":\"\",\"host\":\"www.sina.com.cn\",\"id\":\"%s\",\"tls\":\"none\"}",
					func() string {
						serAddr := os.Getenv("V2Serv")
						if serAddr == "" {
							return GetPulicIP()
						}
						return serAddr
					}(), v.UserPort, v.UserUuid)))
		}
	}
	fmt.Println(clilink)
}

func GetPulicIP() string {
	conn, _ := net.Dial("udp", "8.8.8.8:80")
	defer conn.Close()
	localAddr := conn.LocalAddr().String()
	idx := strings.LastIndex(localAddr, ":")
	return localAddr[0:idx]
}